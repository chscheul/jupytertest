#!/bin/bash
# Variables needed for the kernel building. DO NOT TOUCH!
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
ENV_FILE="environment.yml"
ENV_NAME="ROOT_conda_base"
ORIGIN_ENV="${SCRIPT_DIR}/${ENV_FILE}"

echo "######################################################"
echo "#          DOWNLOADING REQUIRED NTUPLES...           #"
echo "######################################################"
echo ""
mkdir -p ~/data/root_tutorial/
echo "Downloading 'DataEgamma.root' into '~/data/root_tutorial/'..."
curl -o ~/data/root_tutorial/DataEgamma.root https://cernbox.cern.ch/index.php/s/NZHhE7bWySA9sck/download
echo "Downloading 'DataMuons.root' into '~/data/root_tutorial/'..."
curl -o ~/data/root_tutorial/DataMuons.root https://cernbox.cern.ch/index.php/s/MV3jUbiLKwQiB61/download
echo "Downloading 'mc_117049.ttbar_had.root' into '~/data/root_tutorial/'..."
curl -o ~/data/root_tutorial/mc_117049.ttbar_had.root https://cernbox.cern.ch/index.php/s/VNmMx8L5d73v3zX/download
echo "Downloading 'mc_117050.ttbar_lep.root' into '~/data/root_tutorial/'..."
curl -o ~/data/root_tutorial/mc_117050.ttbar_lep.root https://cernbox.cern.ch/index.php/s/3FDhTgjtZ8a9ifZ/download 
echo "Downloading 'mc_147771.Zmumu.root' into '~/data/root_tutorial/'..."
curl -o ~/data/root_tutorial/mc_147771.Zmumu.root https://cernbox.cern.ch/index.php/s/fZO7MlrcOXmwwFN/download
echo "Listing available files in '~/data/root_tutorial/':"
ls -lh ~/data/root_tutorial/
echo ""
echo "######################################################"
echo "#              DOWNLOAD PHASE COMPLETE!              #"
echo "######################################################"
echo -e "\n"
echo "######################################################"
echo "#            COMMENCING KERNEL BUILDING...           #"
echo "######################################################"
echo ""

. /opt/conda/etc/profile.d/conda.sh
echo "Creating Environment ${ENV_NAME}..."
#conda env create -f ${ORIGIN_ENV} -p ~/${ENV_NAME}
echo "Activating Environment..."
conda activate ~/${ENV_NAME}
echo "Generating Kernel..."
python3 -m ipykernel install --user --name ${ENV_NAME} --display-name "Python (ROOT)"
echo "Available Kernels:"
conda deactivate
jupyter kernelspec list

echo -e "\n\n\n\n"
echo "######################################################"
echo "#                  KERNEL FINISHED!                  #"
echo "######################################################"
echo "# Please check whether there is a kernel called      #"
echo "# 'root_conda_base' available in the list above.  If #"
echo "# not, give one of the helpers a shout before you    #"
echo "# continue!                                          #"
echo "#                                                    #"
echo "# IF EVERYTHING WORKED:                              #"
echo "# ---------------------                              #"
echo "# Please consult the docs on how to restart your     #"
echo "# Jupyter server so that you can use the kernel for  #"
echo "# fun and profit!                                    #"
echo "######################################################"
