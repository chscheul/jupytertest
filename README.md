# Welcome to HASCO!
In the following, we will set up your `JupyterHub` instance on GWDG. This will allow you to follow the hands-on tutorials with the very intuitive to use Jupyter notebooks. At the same time, it will also allow you to play around with `cling`, the C++ interpreter coming with the `ROOT` data analysis framework used extensively in particle physics.

But first things first... Let's get you set up!

## In case of problems
We have tried to make the set-up of the software needed for the hands-on sessions as easy to use as possible. Nevertheless, if you ever feel stuck in the course of this set-up session, please don't panic! Just ask one of our friendly helpers - that's what they are here for!

<p align="center">
  <figure>
  <img src="https://imgs.xkcd.com/comics/computer_problems.png" />
  <figcaption>[XKCD 722](https://xkcd.com/722/)</figcaption>
  </figure>
</p>


# Step-by-Step Instructions

## Logging into JupyterHub
For the entire set-up (and for the hands-on sessions themselves), we will use a `JupyterHub` instance hosted by GWDG. The first thing you should therefore do is log right in there and get to know the place!

For this, please do the following:
1. Open [https://jupyter-cloud.gwdg.de/](https://jupyter-cloud.gwdg.de/) in a new tab and log in there with your new shiny Uni Göttingen account
2. Look around! You should now be able to start various programs either as notebooks or from console. We will only be needing `python` and the `Terminal` for the set-up and hands-on sessions.
3. Open a new terminal. This should now open a new tab for you, in which you have access to a normal `bash`-terminal. This will be needed for most of the set-up, while in the practical sessions, you will mostly use `python` notebooks (more on those later).



## Cloning Repository (aka the codebase you are currently looking at)
First, you should really download (or `clone`, in the GitLab lingo) this repository holding all the set-up scripts, configuration files, and the notebooks for testing and the hands-on sessions. To get all that good stuff, please follow these steps:

1. Type `pwd`. This will return the working directory to you (in fact, pwd stands for 'print working directory' - but people using command-line interfaces are lazy and have abbreviated it...). You should currently be in the directory '/home/joyvan/', which is your home directory (also abbreviated as `~` or `$HOME`).
2. Type `ls` - short for 'list' (laziness strikes again). This will print you an overview of everything currently living in your home directory.
3. Type `git clone https://gitlab.cern.ch/chscheul/jupytertest.git ~/HASCO_tutorial`. Here, `git` is a version control software that we used to set all the code up for you. At the same time `clone` tells git to get the repository you are currently looking at (yup, that's the one hiding behind that URL) into the folder `HASCO_tutorial` in your home directory.

Congratulations! Now you are the proud owner of your own git repository containing all the data you will need for the hands-on sessions. Next, we will see how you can actually use this to set-up your `python`-notebooks to be able to use `ROOT`.
